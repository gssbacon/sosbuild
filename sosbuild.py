#!/usr/bin/python
#
# Copyright 2013 Redhat Inc.
# Written by John H. Dulaney <jdulaney@redhat.com>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# SOSbuild
#
# Build a reproducer vm based on information gathered from SOS report.

import sys
import traceback
import os
import logging
import argparse
import re
import time
import pykickstart
import string
import sosbuild
import subprocess
from pykickstart.parser import *
from pykickstart.version import makeVersion

def install_vm(name, num_cpus, mac, ram, rhel):

    os.system('virt-install  --connect qemu:///system --nographics -n ' + name + ' -r ' + str(ram) + ' --vcpus=' + str(num_cpus) + ' -w bridge=virbr0,mac=52:54:00:42:18:D4 --disk path=/var/lib/libvirt/images/bacon.img,size=10 -l ' + rhel + ' --initrd-inject=/home/jdulaney/RedHat/test/anaconda-ks.cfg --extra-args="ks=file:/anaconda-ks.cfg ip=dhcp ksdevice=link"')
    return 0

def installed_rpms(directory):

    rpms = ' '
    fdkick = open(directory + '/installed-rpms')
    for line in fdkick:
        rpms = rpms + (line.split(' '))[0] + '\n'

    fdkick.close()

    return rpms

def rhel_vers(directory, sosreport):
    fd = open(directory + '/' + sosreport + 'etc/redhat-release')
    release = (((str(re.findall(r'release.*', fd.read())).strip('[]'))).split())[1]
    fd.close()
    return release

def extract_sosreport(directory, sosreport):
    # subprocess.call(['cp', ' -f ', directory + sosreport, ' /var/tmp/sosbuild/'])
    sosdir = subprocess.check_output(['tar', 'tfP', directory + '/' + sosreport])
    subprocess.check_output(['tar', 'xf', directory + '/' + sosreport, '-C', directory])
    sosdir = sosdir.splitlines()
    return sosdir[0]

def create_ks():
    defaults = ["install", "cmdline", "lang en_US.UTF-8", "keyboard us", "rootpw --plaintext bacon", "firewall --service=ssh", "authconfig --enableshadow --passalgo=sha512", "selinux --enforcing", "timezone --utc America/New_York", "zerombr", "clearpart --all --initlabel", "bootloader --location=mbr --driveorder=vda --append='crashkernel=auto rhgb quiet'", "part /boot --fstype=ext4 --size=500", "part pv.253002 --grow --size=1", "volgroup vg_rhel6 --pesize=4096 pv.253002", "logvol / --fstype=ext4 --name=lv_root --vgname=vg_rhel6 --grow --size=1024 --maxsize=51200", "logvol swap --name=lv_swap --vgname=vg_rhel6 --grow --size=2016 --maxsize=2016", "reboot"]

    return defaults


def main(sosreport, exit_code):
    # print sosreport

    # first, parse input
    if "/" not in sosreport:
        # Looks like we're in current directory
        directory = subprocess.check_output(['pwd'])
    else:
        if sosreport[0] == '/':
            directory = sosreport[:(sosreport.find('sosreport-') - 1)]
            sosreport = sosreport[(sosreport.find('sosreport-')):]
        else:
            if sosreport[0:1] == './':
                sosreport = sosreport[2:]
            directory = sosreport[:(sosreport.find('sosreport-') - 1)]
            sosreport = sosreport[(sosreport.find('sosreport-')):]
            directory = subprocess.check_output(['pwd']) + directory
    directory = directory.rstrip('\n')
    sosreport = extract_sosreport(directory, sosreport)
    rpm_list = installed_rpms(directory + '/' + sosreport)
    print rpm_list
    print rhel_vers(directory, sosreport)


    # install_vm('test', 1, '52:54:00:42:18:D4', 1024, 'http://download.devel.redhat.com/released/RHEL-6/6.4/Server/x86_64/os')

    return exit_code



# Parse command line args
parser = argparse.ArgumentParser()
parser.add_argument("sosreport", help="sosreport for setting up reproduction system")

args = parser.parse_args()

if __name__ == "__main__":
    try:
        main(args.sosreport, exit_code=True)
    except KeyboardInterrupt, e:
        print >> sys.stderr, _("\n\nExiting on user cancel.")
        sys.exit(1)
