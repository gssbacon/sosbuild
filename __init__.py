#!/usr/bin/python
#
# Copyright 2013 Redhat Inc.
# Written by John H. Dulaney <jdulaney@redhat.com>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# SOSbuild
#
# Build a reproducer vm based on information gathered from SOS report.

import gettext

gettext_dir = "/usr/share/locale"
gettext_app = "sosbuild"

gettext.bindtextdomain(gettext_app, gettext_dir)

__version__ = 0.1
def _sosbuild(msg):
        return gettext.dgettext(gettext_app, msg)

